# Stuff left to do

* basic LaTeX editing for black circle, jewels, dragon
* Howard often ends quotes with --- is that appropriate?
  (see [phf] comments in source)
* Howard often leaves out closing quotes when a monologue
  drags on over several paragraphs; is that really the
  correct convention typographically?
  (see [phf] comments in source)
* Howard often has paragraphs that are not even one line
  long; seems appropriate story-wise but looks strange?
* need to figure out text area for 6x9 paper once and for
  all
* need to modify chapter/section headings; I like memoir
  thatcher for chapters but their's seems too small; less
  whitespace before chapters would be good though
* some stories have sections but not section titles; these
  were probably set as lettrines in the original publications
  and I guess we could have two kinds of section headings,
  one of which is just the number as an inset; bigger problem
  is what to do in the running head when there's no actual
  section title
* speaking of running heads, those need to get fixed too,
  all caps doesn't seem quite right?
* lots of stuff I am forgetting I am sure...
